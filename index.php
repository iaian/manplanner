<?php
// Site config
include_once('config/config.php');

if (!isset($_SESSION['username'])) {
  // Not logged in
  // Include sign up form
  $mainContent = file_get_contents('templates/signUpOrLogin.html');
} else {
  // Logged in
  $mainContent = requireToVar('templates/manPlanner.php');
  $eventsJSON = fetchEvents();

  // Build js for insertion into template
  $scriptContent = '<script type="text/javascript">'.
                      'var username = "'.$_SESSION["nickname"].'",'.
                          'userEmail = "'.$_SESSION["username"].'", '.
                          'eventsJSON = '.$eventsJSON.','.
                          'isAdmin = '.$_SESSION["isAdmin"].';'.
                    '</script>';
}


// Main template
include_once('templates/main.php');
