<?php
$dbRequired = true;

// Site config
include('../config/config.php');

// If not user or admin - quit
if (empty($_SESSION['username']) || $_SESSION['isAdmin'] != 1) {
  exit();
}

$action = $_POST['action'];
$eventId = $_POST['eventId'];

if (empty($action) || empty($eventId)) {
  exit();
}

if ($action === 'approve') {
  // Approve this event id
  // Data to update with
  $data = array( 'thisEventId' => $eventId, 'isChosenEvent' => 1 );

} else {
  // Unapprove this event id
  // Data to update with
  $data = array( 'thisEventId' => $eventId, 'isChosenEvent' => 0 );
}

// Push to db
try {
  $STH = $DBH->prepare("UPDATE events SET isChosenEvent=:isChosenEvent WHERE id = :thisEventId");
  $STH->execute($data);
} catch (PDOException $e) {
  error_log($e->getMessage());
}

// Return new eventsJson to UI
$eventsJSON = fetchEvents();
echo $eventsJSON;