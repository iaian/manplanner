<?php
$dbRequired = true;

// Site config
include('../config/config.php');

// If not logged in quit
if (empty($_SESSION['username'])) {
  exit();
}

$newComment = trim($_POST['comment']);
$eventId = $_POST['eventId'];

if (empty($newComment) || empty($eventId)) {
	exit();
}

// Fetch existing comments for this event id
// Get current vote from DB
$STH = $DBH->query('SELECT comments FROM events WHERE id = '.$eventId);
$STH->setFetchMode(PDO::FETCH_ASSOC);

$row = $STH->fetch();

$existingCommentsArray = json_decode($row['comments']);

$newCommentArray = array(	"username" => $_SESSION['username'],
											  	"nickname" => $_SESSION['nickname'],
											  	"comment"  => $newComment,
												);

// No comments exist in db yet
if ($existingCommentsArray === null) {
	$newCommentArray = array($newCommentArray);
	$newCommentJSON = json_encode($newCommentArray);
} else {
	// Merge with comments in db, new comments on end of array
	array_push($existingCommentsArray, $newCommentArray);
	$newCommentJSON = json_encode($existingCommentsArray);
}

// Update db row with the newCommentJSON
$data = array( 'comments' => $newCommentJSON );

// Push to db
try {
  $STH = $DBH->prepare("UPDATE events SET comments=:comments WHERE id = ".$eventId);
  $STH->execute($data);
} catch (PDOException $e) {
  error_log($e->getMessage());
}

$eventsJSON = fetchEvents();
echo $eventsJSON;