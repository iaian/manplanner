<?php
$dbRequired = true;

// Site config
include('../config/config.php');

// If not logged in quit
if (empty($_SESSION['username'])) {
  exit();
}

$newPw = $_POST['pw'];

// insure newPw isn't empty or less than 6 characters
if (empty($newPw) || strlen($newPw) < 6) {
  exit();
}

// hash newPw
$newPw = hashAndSaltPw($newPw);

$data = array(  'username' => $_SESSION['username'],
                'newPw' => $newPw
              );

// Push to db
try {
  $STH = $DBH->prepare("UPDATE users SET pw=:newPw WHERE username =:username");
  $STH->execute($data);

  echo 'success';
} catch (PDOException $e) {
  error_log($e->getMessage());

  echo 'fail';
}