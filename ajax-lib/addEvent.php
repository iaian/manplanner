<?php
$dbRequired = true;

// Site config
include('../config/config.php');

// If not logged in quit
if (empty($_SESSION['username'])) {
  exit();
}

$title = $_POST['title'];
$date = $_POST['date'];
$body = $_POST['body'];
$isEventIdea = $_POST['isEventIdea'];

// Convert date to unix timestamp
$date = strtotime($date);

if ($isEventIdea) {
  // Is event idea
  // Data to push into db
  $data = array(  'username' => $_SESSION['username'],
                  'date' => $date,
                  'title' => $title,
                  'body' => $body,
                  'isChosenEvent' => '0',
                );
} else {
  // Is approved event
  // Data to push into db
  $data = array(  'username' => $_SESSION['username'],
                  'date' => $date,
                  'title' => $title,
                  'body' => $body,
                  'isChosenEvent' => '1',
                );
}

// Edit Event
if (isset($_POST['idToEdit'])) {
  // add id to edit to data
  $data['idToEdit'] = $_POST['idToEdit'];
  // remove unsued value from array
  unset($data['isChosenEvent']);

  // Push to db
  try {
    // Allow admin to edit any event
    if (isset($_SESSION['isAdmin']) && $_SESSION['isAdmin'] === 1) {
      // Prevent Invalid perameter error
      unset($data['username']);

      $STH = $DBH->prepare("UPDATE events SET timestamp=:date, title=:title, body=:body WHERE id = :idToEdit");
    } else {
      $STH = $DBH->prepare("UPDATE events SET timestamp=:date, title=:title, body=:body WHERE id = :idToEdit AND username = :username");
    }

    $STH->execute($data);

    echo 'success';
  } catch (PDOException $e) {
    error_log($e->getMessage());

    echo 'fail';
  }
  exit();
}

// Push to db
try {
  $STH = $DBH->prepare("INSERT INTO events (username, timestamp, title, body, isChosenEvent) value (:username, :date, :title, :body, :isChosenEvent)");
  $STH->execute($data);

  echo 'success';
} catch (PDOException $e) {
  error_log($e->getMessage());

  echo 'fail';
}