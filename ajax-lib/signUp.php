<?php
$dbRequired = true;

// Site config
include('../config/config.php');

$username = $_POST['username'];
$nickname = $_POST['nickname'];
$pw = $_POST['pw'];

// If strings are empty quit
if (empty($username) || empty($pw) || empty($nickname)) {
  exit();
}

// Hash & salt password
$pw = hashAndSaltPw($pw);

// Data to push into db
$data = array( 'username' => $username, 'pw' => $pw, 'nickname' => $nickname );

// Push to db
try {
  $STH = $DBH->prepare("INSERT INTO users (username, pw, nickname) value (:username, :pw, :nickname)");
  $STH->execute($data);

  $_SESSION['username'] = $username;
  $_SESSION['nickname'] = $nickname;
  $_SESSION['isAdmin'] = 0;

  echo 'success';
} catch (PDOException $e) {
  error_log($e->getMessage());

  echo 'fail';
}