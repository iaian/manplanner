<?php
$dbRequired = true;

// Site config
include('../config/config.php');

// If not logged in quit
if (empty($_SESSION['username'])) {
  exit();
}

// Refresh count
// - for single post ( for a vote )
// - for all posts
if (isset($_POST['getCount'])) {
	exit();
}

// Set a vote
$eventId = mysql_real_escape_string($_POST['eventId']);
$newVote = $_POST['vote'];

if (isset($eventId) && isset($newVote)) {
	// Get current vote from DB
	$STH = $DBH->query('SELECT votesFor, votesAgainst FROM events WHERE id = '.$eventId);
	$STH->setFetchMode(PDO::FETCH_ASSOC);

	$row = $STH->fetch();

	// Unserialize arrays
	$votesFor = json_decode($row['votesFor']);
	$votesAgainst = json_decode($row['votesAgainst']);

	// Cast vote
	// - add or remove the username from the votesFor or votesAgainst arrays
	if ($newVote < 0) {
		// voteAgainst
		// Remove username from votesFor array
		if (is_array($votesFor)) {
			$votesFor = array_diff($votesFor, array($_SESSION['username']));
		}

		// Add username to voteFor array if does not exist
		$addUserName = true;

		// Check array
		if (is_array($votesAgainst)) {
			foreach ($votesAgainst as $key => $value) {
				if ($value === $_SESSION['username']) {
					$addUserName = false;
				}
			}
			// Add to array
			if ($addUserName === true) {
				array_push($votesAgainst, $_SESSION['username']);
			}
		} else {
			$votesAgainst = array($_SESSION['username']);
		}

		// Since this is a down vote we do not need to test approval
		$testEventApproval = false;

		// Serialize arrays
		$votesFor = json_encode($votesFor);
		$votesAgainst = json_encode($votesAgainst);

		// Update DB with new array data
		$data = array( 'votesFor' => $votesFor, 'votesAgainst' => $votesAgainst );

		// Push to db
		try {
		  $STH = $DBH->prepare("UPDATE events SET votesFor=:votesFor, votesAgainst=:votesAgainst WHERE id = ".$eventId);
		  $STH->execute($data);
		} catch (PDOException $e) {
		  error_log($e->getMessage());
		}
	} else {
		// VoteFor
		// Remove username from votesAgainst array
		if (is_array($votesAgainst)) {
			$votesAgainst = array_diff($votesAgainst, array($_SESSION['username']));
		}

		// Add username to voteFor array if does not exist
		$addUserName = true;

		// Check array
		if (is_array($votesFor)) {
			foreach ($votesFor as $key => $value) {
				if ($value === $_SESSION['username']) {
					$addUserName = false;
				}
			}
			// Add to array
			if ($addUserName === true) {
				array_push($votesFor, $_SESSION['username']);
			}
		} else {
			$votesFor = array($_SESSION['username']);
		}

		// For approval of events
		$testEventApproval = true;

		// Serialize arrays
		$votesFor = json_encode($votesFor);
		$votesAgainst = json_encode($votesAgainst);

		// Update DB with new array data
		$data = array( 'votesFor' => $votesFor, 'votesAgainst' => $votesAgainst );

		// Push to db
		try {
		  $STH = $DBH->prepare("UPDATE events SET votesFor=:votesFor, votesAgainst=:votesAgainst WHERE id = ".$eventId);
		  $STH->execute($data);
		} catch (PDOException $e) {
		  error_log($e->getMessage());
		}
	}

	// Get updated count for json return
	$STH = $DBH->query('SELECT votesFor, votesAgainst FROM events WHERE id = '.$eventId);
	$STH->setFetchMode(PDO::FETCH_ASSOC);

	$row = $STH->fetch();

	$row['votesFor'] = json_decode($row['votesFor']);
	$row['votesAgainst'] = json_decode($row['votesAgainst']);

	// Get current count
	$votesForCount = count($row['votesFor']);
	$votesAgainstCount = count($row['votesAgainst']);

	$voteCount = $votesForCount - $votesAgainstCount;
	$newVoteCount = $voteCount;

	/*
	 * This code auto approved users based upon the number of users in the db

	if ($testEventApproval ) {
		// Must have been upvote
		// First we need a count of all the users in table users
		$STH = $DBH->query('SELECT username FROM users');
		$STH->setFetchMode(PDO::FETCH_ASSOC);

		$row = $STH->fetchAll();

		// This is the total number of users
		$usersCount = count($row);

		// Test to see if event should be approved
		if (($votesForCount / $usersCount) > 0.5) {
			// Aprrove Event
			$STH = $DBH->prepare('UPDATE events SET isChosenEvent=1 WHERE id = '.$eventId);
			$STH->execute();

			$updateEventsJSON = 'true';
		}
	}
	*/

	if (empty($updateEventsJSON) || !$updateEventsJSON) {
		$updateEventsJSON = 'false';
	}

	echo '{ "status": "success", "updatedVoteCount": '.$newVoteCount.', "updateUI": "'.$updateEventsJSON.'"}';
	// echo 'success';
}