<?php
$dbRequired = true;

// Site config
include('../config/config.php');

$username = $_POST['username'];
$pw = $_POST['pw'];

// If strings are empty quit
if (empty($username) || empty($pw)) {
  exit();
}

// Prepare query
$STH = $DBH->prepare('SELECT username, pw, nickname, isAdmin from users WHERE username = :username');
$STH->execute(array(':username' => $username));

// Fetch data
$STH->setFetchMode(PDO::FETCH_ASSOC);
$row = $STH->fetch();

if (empty($row)) {
  // No row exists
  echo 'fail';
  exit();
} else {
  // Username is valid now - test password
  $pw = hashAndSaltPw($pw);
  $existingPw = $row['pw'];

  if ($pw === $existingPw) {
    // Password is correct
    $_SESSION['username'] = $username;
    $_SESSION['nickname'] = $row['nickname'];

    // Insure $_SSSION['isAdmin'] is set correctly
    $_SESSION['isAdmin'] = 0;

    if ($row['isAdmin'] === "1") {
      $_SESSION['isAdmin'] = 1;
    }

    echo 'success';
  } else {
    echo 'fail';
  }
}