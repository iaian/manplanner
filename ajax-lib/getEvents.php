<?php
$dbRequired = true;

// Site config
include('../config/config.php');

// If not logged in quit
if (empty($_SESSION['username'])) {
  exit();
}

$eventsJSON = fetchEvents();
echo $eventsJSON;
