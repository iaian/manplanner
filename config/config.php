<?php
// Start session
if (empty($_SESSION)) {
  session_start();
}

// Basic site config
$siteTitle = 'ManPlanner';

// For hashing passwords
$SALT = 'h7dBjDykUKPYsaHzwbfP4cMidojYZUvCsxBuVsEbuXc';

// Check for site-config.php for DB connection info
$rootPath = $_SERVER['DOCUMENT_ROOT'];
$siteConfigPath = $rootPath.'/site-config.php';

// Test for existance of /site-config.php
//  - if not found use DEV settings
if (file_exists($siteConfigPath)) {
  // PRODUCTION
  include_once($rootPath.'/site-config.php');
} else {
  // DEVELOPMENT
	$HOST = 'localhost';
	$DB_NAME = 'manPlanner';
	$DB_USER = 'root';
	$DB_PASS = 'root';
}


/* DB CONNECTIONS - PDO */

// Connect MySQL with PDO_MYSQL using variable $dbRequired = true;
if (empty($dbRequired)) { $dbRequired = false; }

if ($dbRequired) {
  try {
    $DBH = new PDO("mysql:host=$HOST;dbname=$DB_NAME", $DB_USER, $DB_PASS);
    $DBH->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
  } catch (PDOException $e) {
    error_log('Connection failed: ' . $e->getMessage());
  }
} else {
  $DBH = null;
}

// Enabling db via function
function requreDb($bool) {
  global $HOST, $DB_NAME, $DB_USER, $DB_PASS;

  if ($bool) {
    try {
      $DBH = new PDO("mysql:host=$HOST;dbname=$DB_NAME", $DB_USER, $DB_PASS);
      $DBH->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    } catch (PDOException $e) {
      error_log('Connection failed: ' . $e->getMessage());
    }
    return $DBH;
  } else {
    $DBH = null;
  }
}

// Include common functions
include_once('functions.php');
