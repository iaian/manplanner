<?php
/* FUNCTIONS
 */

function hashAndSaltPw($thisPw) {
  global $SALT;

  $hashedPass = crypt($thisPw, '$2a$12$' . $SALT);
  return $hashedPass;
}

function requireToVar($file){
  // Include variables that are needed in page here so they will be accessible
  global $siteTitle;

  ob_start();
  require($file);
  return ob_get_clean();
}

function fetchEvents() {
  // This function will fetch all events then return a json object containing all data

  // Vars for grouping events on same date
  $lastDate = null;
  $previousEventIndex = null;
  $activeEventRows = array();

  // Connect DB for operations
  $DBH = requreDb(true);

  // Return the data from low to high
  $STH = $DBH->query('SELECT * FROM events ORDER BY timestamp ASC');
  $STH->setFetchMode(PDO::FETCH_ASSOC);
  $rows = $STH->fetchAll();

  // Json_decode array rows
  foreach ($rows as $key => $value) {
    if ($key === 0) {
      $lastDate = $rows[$key]['timestamp'];
      $previousEventIndex = $key;
    }

    $rows[$key]['votesFor'] = json_decode($value['votesFor']);
    $rows[$key]['votesAgainst'] = json_decode($value['votesAgainst']);

    // Add count arrays
    $votesForCount = count($rows[$key]['votesFor']);
    $votesAgainstCount = count($rows[$key]['votesAgainst']);

    // Ammend vote count
    $rows[$key]['voteCount'] = $votesForCount - $votesAgainstCount;

    // Fix comments
    $rows[$key]['comments'] = json_decode($rows[$key]['comments']);

    // Test for activeEvents
    if ($rows[$key]['isChosenEvent'] == 1) {
      // Error_log('this is chosen event');
      // Add this row to array and remove it from $rows
      array_push($activeEventRows, $rows[$key]);
      unset($rows[$key]);
    }

    // For grouping evnents by same timestamp for re ordering based upon voteCount
    if (
      isset($rows[$key]) &&
      $lastDate === $rows[$key]['timestamp']
    ) {
      // Detects new high vote
      if ($rows[$key]['voteCount'] > $rows[$previousEventIndex]['voteCount']) {
        // Replace previous index wit this row
        $oldRow = $rows[$previousEventIndex];

        $rows[$previousEventIndex] = $rows[$key];
        $rows[$key] = $oldRow;
      }

    } else {
      // If the timestamps do not match it must be a new date
      $previousEventIndex = $key;
    }


    if (isset($rows[$key])) {
      // Used to group events
      $lastDate = $rows[$key]['timestamp'];
    } else {
      $lastDate = 0;
    }
  }

  // Puts active events at front of array
  if (count($activeEventRows) > 0) {
    $rows = array_merge($activeEventRows, $rows);
  }

  // Encode array to json
  $eventsJSON = json_encode($rows);

  return $eventsJSON;
}
