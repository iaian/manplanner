# ManPlanner - a homies fun day planning tool

## How to run and begin development

### clone repo & setup webroot

1. Clone repo to your local machine.
2. Insure the cloned folder is inside of your webroot, or setup a virtual host to point to the cloned directory.

### Node js & Grunt js

1. Now you need to insure node.js is installed, if not get that installed ( I am currently using node version v0.10.20 ).
2. After node is installed you need to install Node Package Manager or npm.
3. Install all the dependencies using npm install.
4. Open a terminal window open at the root of the cloned folder.
5. Run command `npm install`, this installs all the node dependencies ( mostly grunt.js related stuff )
6. Now you need to run `grunt watch` in the terminal to have grunt watch your css and js files for changes so that the combined and concatenated files are updated when you make changes. To minify, concatenate, and combine all js and css manually run "grunt" in terminal.

### MYSQL

1. You will need to know your hostname, db name ( manPlanner ), and db username, and password.
2. Set these settings in `/config/config.php` - look for "// DEVELOPMENT" and set your settings below that.
3. Now you need to update the mysql db.
4. First create db - manPlanner if does not exist already
5. Now you need to run the sql in - `/sql-scaffold/manPlanner.sql`
6. You are done - you should have a empty table structure ready for data.
7. Rename site-config-sample.php to site-config.php and fill in database values

## You are ready to begin development

## Quickstart Guide (assumes you have node already installed)

1. Clone Repo
2. Setup Webroot
3. Install Node depenancies with - `$ npm install`
4. Create DB named - manPlanner
5. Extract sql from `/sql-scaffold/manPlanner.sql` to manPlanner mysql DB
6. Run grunt watch - `$ grunt watch`
7. Open a web browser to the project root
8. Rename `/site-config-sample.php` to `/site-config.php` and fill in database values.

Hopefully this isn't too confusing! Let me know if you have problems : [iaianrw@gmail.com](mailto:iaianrw@gmail.com)

-----------------------------
Online Production Version of this app is now launched at - [http://manplanner.theiaian.com](http://manplanner.theiaian.com)