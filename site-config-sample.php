<?php
/* Site Config Sample
 *  HOWTO : this file must be renamed to work
 *	Rename the file to : site-config.php
 *
 * 	For more info see - config/config.php
 */

$siteTitle = 'ManPlanner';

$HOST = 'localhost';
$DB_NAME = 'manPlanner';
$DB_USER = 'root';
$DB_PASS = 'root';
