<?php
// Insure only logged in users can see this page
if (empty($_SESSION['username'])) {
  exit();
}
?>

<div class="well manPlanWell">
  <div class="btn-group mainWellTopRightBtnGroup">
    <button type="button" class="btn btn-primary btnShowUserModal">User <span class="glyphicon glyphicon-user"></span></button>
    <button type="button" class="btn btn-danger btnLogout">Logout</button>
  </div>

  <h1><?php echo $siteTitle ?></h1>

  <div class="carouselNoItems"></div>
  <div id="carousel-events" class="carousel slide">
    <ol class="carousel-indicators"></ol>
    <div class="carousel-inner"></div>

    <a class="left carousel-control" href="#carousel-events" data-slide="prev">
      <span class="icon-prev"></span>
    </a>
    <a class="right carousel-control" href="#carousel-events" data-slide="next">
      <span class="icon-next"></span>
    </a>
  </div>
</div>

<div id="eventIdeas" class="well">
  <h3>Event Ideas</h3>

  <button class="btn btn-xs toggleOldEventsBtn hide">Show Old Events</button>

  <div id="eventIdeaList"></div>

  <div class="btn-group pull-right mainWellBottomBtnGroup">
    <button class="btn btn-primary addNewEventIdea">Add Event Idea</button>
    <button class="btn btn-info toggleChatDisplay active">
      <span class="displayVerb">Hide</span> Chat
      <span class="glyphicon glyphicon-chevron-up"></span>
    </button>
  </div>
</div>

<div id="chatDiv" class="well">

  <div id="presenceDiv">
    <h5>Online Users</h5>
  </div>

  <div id='messagesDiv'></div>

  <div id="messagesInput" class="input-group">
    <input type='text' class='form-control' id='messageInput' placeholder='Message...'>
    <span class="input-group-btn">
      <button class="btn btn-default" id="sendChatMessage" type="button">Send Message</button>
    </span>
  </div>

</div>