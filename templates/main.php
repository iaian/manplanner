<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <title><?php echo $siteTitle; ?></title>

    <link href="dist/manPlanner.min.css" rel="stylesheet">

    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="assets/js/html5shiv.js"></script>
      <script src="assets/js/respond.min.js"></script>
    <![endif]-->
    <script src="https://cdn.firebase.com/v0/firebase.js"></script>
  </head>
  <body>

    <div id="mainAlert" class="alert hide"></div>

    <div id="mainWell" class="well">
      <?php echo $mainContent; ?>
    </div>

    <audio id="audioTag"></audio>

    <div id="mainModal" class="modal fade">
      <div class="modal-dialog">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
            <h4 class="modal-title"></h4>
          </div>
          <div class="modal-body"></div>
          <div class="modal-footer"></div>
        </div>
      </div>
    </div>

    <?php
      /* Intended use - to place data into page to be accessable via JS
       * Example : $scriptContent = '<script type="text/javascript">'.
       *                              'var username = "'.$_SESSION["nickname"].'";'.
       *                            '</script>';
       *
       * Note - vanilla js only ( as jquery is not yet included )
       */

      if (isset($scriptContent)) {
        echo $scriptContent;
      }
    ?>

    <script src="//ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>
    <script>window.jQuery || document.write('<script src="js/vendor/jquery-1.9.1.min.js"><\/script>')</script>
    <script src="dist/manPlanner.min.js"></script>

  </body>
</html>
