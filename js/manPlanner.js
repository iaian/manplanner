/* GLOBAL VARS
 */

var audioElement = document.getElementById('audioTag'),
    imRecievedSoundPath = 'sounds/bcfire01.wav',
    canPlaySound = false,
    // note: this timestamp is one day behind the real current time to avoid timezone issues
    currentUnixTime = (parseInt(new Date().getTime() / 1000, 10) - (24 * 60 * 60)),
    // chat users array - for coloring usernames
    messagesRef = new Firebase('https://manplanner.firebaseio.com/webchat'),
    activeUsers = {};


/* INITIALIZERS
 */

// test for eventsJson & username
if (typeof eventsJSON !== "undefined" || typeof username !== "undefined") {
  initializeEventsHTML();
  initializeFirebaseChat();
  initializeUserPresence();
}

$( document ).ready(function() {
  // Chat display
  $('.toggleChatDisplay').click(function() {
    $(this).toggleClass('active');

    if ($(this).hasClass('active')) {
      // Show Chat
      $(this).children('.displayVerb').text('Hide');
      $(this).children('.glyphicon').removeClass().addClass('glyphicon glyphicon-chevron-up');
    } else {
      // Hide Chat
      $(this).children('.displayVerb').text('Show');
      $(this).children('.glyphicon').removeClass().addClass('glyphicon glyphicon-chevron-down');
    }
    $('#chatDiv').slideToggle();
  });

  // Voting
  $('body').delegate('.voteContain .voteIcon', "click", function() {
    var eventId = $(this).parent().attr('data-eventId'),
        thisVote = null;

    // Switch vote indicators for only this event
    $(this).parent().children('.hasVote').removeClass('hasVote');
    $(this).addClass('hasVote');

    if ($(this).hasClass('upVote')) {
      // upVote
      thisVote = 1;
    } else {
      // downVote
      thisVote = -1;
    }
    submitVote(eventId, thisVote);
  });

  // Comments
  $('body').delegate('.commentContainer', "click", function() {
    var eventId = $(this).parent().attr('data-eventId'),
        indexNum = $(this).parent().attr('data-indexnum');

    // build & show comments for eventId
    buildAndShowComments(indexNum, eventId);
  });

  // Add new comment
  $('body').delegate('.AddCommentBtn', "click", function() {
    var thisEventId = $(this).attr('data-eventId'),
        thisCommentText = $('#newCommentInput').val(),
        thisIndexNum = $(this).attr('data-indexNum');

    submitNewComment(thisEventId, thisCommentText, thisIndexNum);

    // clear input
    $('#newCommentInput').val('');
  });

  // Prevent comment form from normal submission
  $('body').delegate('#addCommentForm', "submit", function(event) {
    event.preventDefault();
    $('.AddCommentBtn').click();
  });

  // New event
  $('.addNewEvent').click(function() {
    buildAndShowNewEventModal();
  });

  // New event idea
  $('.addNewEventIdea').click(function() {
    buildAndShowNewEventModal(true);
  });

  // Prevent Event form from submitting normally
  $('#addEventForm').submit(function(event) {
    event.preventDefault();
  });

  // Submit Event Idea
  $('body').delegate('.submitNewEvent', "click", function() {
    var title = $('#eventTitle').val(),
        date = $('#eventDate').val(),
        body = $('#eventBody').val();

    if (title !== '' && date !== '' && body !== '') {
      $('#mainModal .modal-body .alert').remove();

      // Submit Event Idea
      submitEvent(title, date, body, true);
    } else {
      $('#mainModal .modal-body').append('<div class="alert alert-danger">Please Fill In All Inputs to Submit Form</div>');
    }
  });

  // Submit Edit Event
  $('body').delegate('.submitEventEdit', "click", function() {
    var title = $('#eventTitle').val(),
        date = $('#eventDate').val(),
        body = $('#eventBody').val(),
        thisEventId = $(this).attr('data-eventId');

    if (title !== '' && date !== '' && body !== '') {
      $('#mainModal .modal-body .alert').remove();

      // Submit Event Idea
      submitEvent(title, date, body, true, thisEventId);
    } else {
      $('#mainModal .modal-body').append('<div class="alert alert-danger">Please Fill In All Inputs to Submit Form</div>');
    }
  });

  // Old events toggle
  $('.toggleOldEventsBtn').click(function() {
    $(this).toggleClass('active');

    if ($(this).hasClass('active')) {
      $(this).text('Hide Old Events');
    } else {
      $(this).text('Show Old Events');
    }

    if ($('.hidenEventIdea').hasClass('hide')) {
      $('.hidenEventIdea').hide().removeClass('hide');
    }

    $('.hidenEventIdea').fadeToggle();
  });

  // Admin Approval
  $('body').delegate('.adminAprovalBtn', "click", function() {
    var thisAction = $(this).attr('data-approve-action'),
        thisEventId = $(this).parent().parent().attr('data-eventid'),
        thisTimestamp = $(this).parent().parent().attr('data-eventTimestamp');

    // Disallow approval of multiple events on same date
    for (var i = 0; i < eventsJSON.length; i++) {
      // Test for chosen event, matching timestamp, and is aprroval action
      if (
        eventsJSON[i]['isChosenEvent'] === "1" &&
        thisTimestamp === eventsJSON[i]['timestamp'] &&
        thisAction === 'approve'
      ) {
        displayAlert('danger', 'Cannot approve this event, event already chosen for this date.', 5000);
        return '';
      }
    }

    $.post( "ajax-lib/eventActions.php", { eventId: thisEventId, action: thisAction })
      .done(function( data ) {
        // this endpoint returns eventsJSON string
        data = jQuery.parseJSON(data);

        eventsJSON = data;
        initializeEventsHTML();
      });
  });

  // Edit Event
  $('body').delegate('.editThisEventBtn', "click", function() {
    var thisEventId = $(this).parent().parent().attr('data-eventid');

    buildAndShowNewEventModal(false, true, thisEventId);
  });

  // Launch User Modal - user settings
  $('.btnShowUserModal').click(function() {
    buildAndShowUserModal();
  });

  // Logout
  $('.btnLogout').click(function() {
    $.post( "ajax-lib/logout.php" )
      .done(function() {
        window.location = '';
      });
  });

}); // End $DRF

/* FUNCTIONS
 */

/* User Modal */
function buildAndShowUserModal() {
  var modalTitle = 'User Settings',

      bodyContent = '<p>Reset your password</p>' +
                    '<form id="resetPasswordForm" class="form-horizontal" role="form">' +
                      '<div class="form-group">' +
                        '<label for="passwordReset1" class="col-xs-4 control-label">New Password</label>' +
                        '<div class="col-xs-7">' +
                          '<input type="password" class="form-control" id="passwordReset1" placeholder="New Password" value="">' +
                        '</div>' +
                      '</div>' +
                      '<div class="form-group">' +
                        '<label for="passwordReset2" class="col-xs-4 control-label">Confirm Password</label>' +
                        '<div class="col-xs-7">' +
                          '<input type="password" class="form-control" id="passwordReset2" placeholder="Confirm Password" value="">' +
                        '</div>' +
                      '</div>' +
                    '</form>' +
                    '<div class="userSettingsAlert"></div>',

      footerContent = '<button type="button" class="btn btn-default btn-lg pull-left" data-dismiss="modal">Close</button>' +
                      '<button type="button" class="btn btn-primary btn-lg SubmitUserSettingsBtn">Save Settings</button>';

  // inject into modal
  $('#mainModal .modal-title').html(modalTitle);
  $('#mainModal .modal-body').html(bodyContent);
  $('#mainModal .modal-footer').html(footerContent);

  // show modal
  $('#mainModal').modal('show');

  $('.SubmitUserSettingsBtn').attr('disabled', 'disabled');

  $('#passwordReset1, #passwordReset2').keyup(function() {
    var pw1 = $('#passwordReset1').val(),
        pw2 = $('#passwordReset2').val();

    if (pw1 === '') {
      $('.SubmitUserSettingsBtn').attr('disabled', 'disabled');
      $('.userSettingsAlert').addClass('alert alert-danger').html('Password cannot be blank!');
      return '';
    }

    if (pw1 === pw2) {
      $('.SubmitUserSettingsBtn').removeAttr('disabled');
      $('.userSettingsAlert').removeClass().addClass('userSettingsAlert').html('');
    } else {
      $('.SubmitUserSettingsBtn').attr('disabled', 'disabled');
      $('.userSettingsAlert').addClass('alert alert-danger').html('Passwords do not match!');
      return '';
    }

    if (pw1.length < 6 || pw2.length < 6) {
      $('.SubmitUserSettingsBtn').attr('disabled', 'disabled');
      $('.userSettingsAlert').addClass('alert alert-danger').html('Password must be at least 6 characters long!');
    }
  });

  // Submit user settings form btn
  $('body').delegate('.SubmitUserSettingsBtn', "click", function() {
    var newPw = $('#passwordReset1').val();

    $.post( "ajax-lib/resetPassword.php", { pw : newPw })
      .done(function( data ) {
        $('#mainModal').modal('hide');
        displayAlert('success', '<b>Password Updated</b>', 6000);
      });
  });
}

/* Comments */
function submitNewComment(eventId, commentText, indexNum) {
  if (eventId && commentText) {

    $.post( "ajax-lib/comments.php", { eventId: eventId, comment: commentText })
      .done(function(data) {
        data = jQuery.parseJSON(data);
        eventsJSON = data;

        buildAndShowComments(indexNum, eventId);
        initializeEventsHTML();
      });
  }
}

function buildAndShowComments(indexNum, eventId) {
  // first build comments for this eventId
  var eventComments = eventsJSON[indexNum]['comments'],
      // html for modal
      modalTitle = eventsJSON[indexNum]['title'] + ' - Comments',
      bodyContent = '',
      footerContent = '<button type="button" class="btn btn-default btn-lg pull-left" data-dismiss="modal">Close</button>' +
                      '<button type="button" class="btn btn-primary btn-lg AddCommentBtn" data-eventId="'+ eventId +'" data-indexNum="'+ indexNum +'">Add Comment</button>',

      // new comment input
      newCommentInput = '<form id="addCommentForm" class="form-horizontal" role="form">' +
                          '<div class="form-group">' +
                            '<div class="col-xs-12">' +
                              '<input type="text" class="form-control" id="newCommentInput" placeholder="Comment">' +
                            '</div>' +
                          '</div>' +
                        '</form>';

  if (eventComments !== null) {
    for (var i = 0; i < eventComments.length; i++) {
      var thisNickName = eventComments[i]['nickname'],
          thisComment = eventComments[i]['comment'];

      bodyContent += '<li id="comment'+ i +'" class="commentRow">' +
                        '<span class="nickname">'+ thisNickName +'</span> - ' +
                        '<span class="commentText">'+ thisComment +'</span>' +
                      '</li>';

    }
  } else {
    bodyContent = '<p> No Comments Yet</p>';
  }

  // wrap body content in ul tag
  bodyContent = '<ul class="commentUL">'+ bodyContent +'</ul>' + newCommentInput;

  // inject into modal
  $('#mainModal .modal-title').html(modalTitle);
  $('#mainModal .modal-body').html(bodyContent);
  $('#mainModal .modal-footer').html(footerContent);

  // show modal
  $('#mainModal').modal('show');
  // focus comment input
  setTimeout(function() {
    $('#newCommentInput').focus();
  }, 250);
}

/* New Event */
function submitEvent(title, date, body, isEventIdea, eventIdToEdit) {
  var updateBody = false;

  if (title && body) {
    // test eventIdToEdit for value

    // Fix body - convert \n to <br/> - note the spaces around the br tag .. needed for the shitty split below
    body = body.replace(/\n/g, " <br/> ");

    // Detect urls and wrap them in a tag
    bodyArray = body.split(' ');
    for (var i = 0; i < bodyArray.length; i++) {

      // detect url
      if (bodyArray[i].match(/(^http[s]?[\d,\w:\/.?=%&]*)/)) {
        // wrap url in a tag
        bodyArray[i] = ' <a class="eventLink" href="'+ bodyArray[i] +'" target="_blank">' + bodyArray[i] + '</a> ';
        updateBody = true;
      }
    }

    if (updateBody === true) {
      body = bodyArray.join(' ');
    }

    if (eventIdToEdit) {
      // Edit event
      $.post( "ajax-lib/addEvent.php", { idToEdit: eventIdToEdit, title: title, date: date, body: body, isEventIdea: false })
        .done(function(data) {
          $('#mainModal').modal('hide');
          if (data === 'success') {
            displayAlert('success', '<b>Event Updated</b>', 6000);

            // update json and refresh UI
            updateEventsJSON(true);
          }
        });
    } else {
      // Normal event create
      $.post( "ajax-lib/addEvent.php", { title: title, date: date, body: body, isEventIdea: isEventIdea })
        .done(function(data) {
          $('#mainModal').modal('hide');
          if (data === 'success') {

            if (isEventIdea) {
              displayAlert('success', '<b>Event Idea Saved</b>', 6000);
            } else {
              displayAlert('success', '<b>Event Saved</b>', 6000);
            }

            // update json and refresh UI
            updateEventsJSON(true);
          }
        });
    }
  }
}

/* Get updated eventsJSON */
function updateEventsJSON(updateUI) {
  // this functions goal is to fetch and update the eventsJSON object
  $.get( "ajax-lib/getEvents.php" )
    .done(function( data ) {
      data = jQuery.parseJSON(data);

      eventsJSON = data;

      // if true update UI
      if (updateUI) {
        initializeEventsHTML();
      }
    });
}

/* Voting */
function submitVote(eventId, vote) {
  if (vote && eventId) {
    $.post( "ajax-lib/vote.php", { eventId: eventId, vote: vote })
      .done(function(data) {
        data = jQuery.parseJSON(data);

        if (data.status === 'success') {
          updateVoteCount(eventId, data.updatedVoteCount);

          // check to see if UI should be refreshed
          if (data.updateUI === 'true') {
            updateEventsJSON(true);
          }
        }
      });
  }
}

function updateVoteCount(eventId, newVote) {
  $('.voteContain').each(function() {
    if ($(this).attr('data-eventId') === eventId) {
      $(this).children('.voteCount').text(newVote);
    }
  });
}

/* Construct and show new & edit events modal */
function buildAndShowNewEventModal(isEventIdea, isEventEdit, editId) {
  // Default values set here
  var submitBtnClass = 'submitNewEvent',
      thisTitle = '',
      thisDate = '',
      thisBody = '',
      modalTitle = 'Add Event',
      eventId = 0;

  // Detect edit modal & change certain vars
  if (isEventEdit) {
    for (var i = 0; i < eventsJSON.length; i++) {
      // If edit id matches this json item then pull data into vars for easy use
      if (eventsJSON[i]['id'] == editId) {
        thisTitle = eventsJSON[i]['title'];
        thisDate = timeConverter(eventsJSON[i]['timestamp'], true);
        thisBody = eventsJSON[i]['body'];
        eventId = editId;
      }
    }

    // Fix body - convert <br/> to \n
    thisBody = thisBody.replace(/<br\s*[\/]?>/g, "\n");

    submitBtnClass = 'submitEventEdit';
    modalTitle = 'Edit Event';
  }

  // Build HTML items
  var footerContent = '<button type="button" class="btn btn-default btn-lg pull-left" data-dismiss="modal">Close</button>' +
                      '<button type="button" class="btn btn-primary btn-lg '+ submitBtnClass +'" data-eventId="'+ eventId +'">Save Event</button>',

      dateFormPart = '<div class="form-group">' +
                        '<label for="eventDate" class="col-xs-3 control-label">Date</label>' +
                        '<div class="col-xs-8">' +
                          '<input type="text" class="form-control" id="eventDate" placeholder="Event Date" value="'+ thisDate +'">' +
                        '</div>' +
                      '</div>';

  // dont show dateform part if isEventIdea
  if (isEventIdea) {
    modalTitle = 'Add Event Idea';
  }

  var bodyContent = '<form id="addEventForm" class="form-horizontal" role="form">' +
                  '<div class="form-group">' +
                    '<label for="eventTitle" class="col-xs-3 control-label">Title</label>' +
                    '<div class="col-xs-8">' +
                      '<input type="text" class="form-control" id="eventTitle" placeholder="Event Title" value="'+ thisTitle +'">' +
                    '</div>' +
                  '</div>' +
                  dateFormPart +
                  '<div class="form-group">' +
                    '<label for="eventBody" class="col-xs-3 control-label">Description</label>' +
                    '<div class="col-xs-8">' +
                      '<textarea id="eventBody" class="form-control">'+ thisBody +'</textarea>' +
                    '</div>' +
                  '</div>' +
                '</form>';

  // inject into modal
  $('#mainModal .modal-title').html(modalTitle);
  $('#mainModal .modal-body').html(bodyContent);
  $('#mainModal .modal-footer').html(footerContent);

  // setup datepicker
  $("#eventDate").datepicker();

  // show modal
  $('#mainModal').modal('show');
  // focus title
  setTimeout(function() {
    $('#eventTitle').focus();
  }, 250);
}

/* Constructing events html */
function initializeEventsHTML() {
  // HOLY SHIT THIS IS A HUGE FUNCTION
  // This takes eventsJSON and builds and injects html for the UI

  var $carousel = $('#carousel-events'),
      // Building html
      carouselIndicatorHTML = '',
      allSlidesHTML = '',
      eventIdeasHTML = '',
      // Active class
      activeClass = '',
      // Admin buttons
      adminCarouselBtns = '',
      adminIdeasBtns = '',
      // Grouping event ideas & storing last event date
      nextEventKey = null,
      lastEventIdeaDate = 0,
      // Counting events and approved events
      approvedEventCount = 0,
      eventIdeaCount = 0,
      // Stores active events
      activeEventDates = [];

  // Prevent carousel from rotating
  $carousel.carousel('pause');

  // Build admin buttons
  if (isAdmin === 1) {
    adminCarouselBtns = '<button type="button" class="btn btn-sm btn-danger adminAprovalBtn"'+
                          ' data-approve-action="unapprove">Unapprove Event</button>';

    adminIdeasBtns = '<button type="button" class="btn btn-sm btn-success adminAprovalBtn"'+
                        ' data-approve-action="approve">Approve Event</button>';
  }

  // Build carousel html & all slides HTML
  for (var i = 0; i < eventsJSON.length; i++) {
    // Adjust timestamp for timezone issues
    // Add 6 hours to timestamp
    thisEventTime = timeConverter(parseInt(eventsJSON[i]['timestamp'], 10) + (6 * 60 * 60));

    // Edit this event
    // Test if this user created this event, or is admin
    if (
      userEmail === eventsJSON[i]['username'] ||
      isAdmin === 1
    ) {
      // Build edit button
      thisEditButton = '<button type="button" class="btn btn-sm btn-info editThisEventBtn">Edit Event</button>';
    } else {
      // Empty edit button
      thisEditButton = '';
    }

    // Determine if this event is approved
    if (eventsJSON[i]['isChosenEvent'] === "1") {
      // Keep count of approved events
      approvedEventCount++;

      // Determines which slide should be active
      if (eventsJSON[i]['timestamp'] > currentUnixTime && nextEventKey === null) {
        activeClass = 'active';
        nextEventKey = i;
      } else {
        activeClass = '';
      }

      // Store this time stamp for use in hiding events matching this date
      activeEventDates.push(thisEventTime);

      // Html for indicator carousel
      carouselIndicatorHTML += '<li data-target="#carousel-events" data-slide-to="'+ i +'" class="'+ activeClass +'"></li>';

      // Main carousel html
      allSlidesHTML += '<div id="slide'+ i +'" class="item '+ activeClass +'">' +
                          '<div class="slideContainer" data-eventId="'+ eventsJSON[i]['id'] +'" data-indexNum="'+ i +'" data-eventTimestamp="'+ eventsJSON[i]['timestamp'] +'">' +
                            // Event
                            '<h4>'+ eventsJSON[i]['title'] +' - <span class="activeEventDate">'+ thisEventTime + '</span></h4>' +
                            '<h6>Event Proposed by - '+ eventsJSON[i]['username'] +'</h6>' +
                            '<p>'+ eventsJSON[i]['body'] +'</p>' +
                            // Admin only buttons
                            '<div class="btn-group">' +
                              thisEditButton +
                              adminCarouselBtns +
                            '</div>' +
                            // COMMENTS & VOTING DISABLED FOR CAROUSEL ITEMS
                          '</div>' +
                       '</div>';
    } else {
      // This is an event idea
      // To trigger hidden items - if is old
      var hideClass = '',
          upVoteIndicatorClass = '',
          downVoteIndicatorClass = '';

      // Vote count indicators
      if ( $.inArray(userEmail, eventsJSON[i]['votesFor']) > -1 ) {
        // User has voted for this event idea
        upVoteIndicatorClass = 'hasVote';
      } else if ( $.inArray(userEmail, eventsJSON[i]['votesAgainst']) > -1 ) {
        // User has voted against this event idea
        downVoteIndicatorClass = 'hasVote';
      }

      // If comments are not null - grab comment count
      if (eventsJSON[i]['comments'] !== null) {
        commentCount = eventsJSON[i]['comments'].length;
      } else {
        commentCount = 0;
      }

      // Build voting html
      voteHTML = '<div class="voteContain" data-eventId="'+ eventsJSON[i]['id'] +'">' +
                    '<span class="voteIcon glyphicon glyphicon-arrow-up upVote '+ upVoteIndicatorClass +'" title="up vote this event"></span>' +
                    '<span class="voteCount">'+ eventsJSON[i]['voteCount'] +'</span>' +
                    '<span class="voteIcon glyphicon glyphicon-arrow-down downVote '+ downVoteIndicatorClass +'" title="down vote this event"></span>' +
                  '</div>';

      // Detect old events
      if (currentUnixTime > parseInt(eventsJSON[i]['timestamp'], 10) + (6 * 60 * 60)) {
        // Set class for hiding this event
        hideClass = 'hide hidenEventIdea';

        // Show old events button - so user can display old event ideas
        $('.toggleOldEventsBtn').removeClass('hide');
      }

      // Header for event idea date - grouping items on same date
      if (lastEventIdeaDate !== thisEventTime) {
        eventIdeasHTML += '<div class="thisEventDateGroup"><h4 class="dateGroup '+ hideClass +'">Events For <span class="eventIdeaDate">'+ thisEventTime +'</span></h4>';
        // For tracking number of ideas
        eventIdeaCount++;
      }

      // Build html for events ideas section
      eventIdeasHTML += '<div class="eventIdeaContain '+ hideClass +'" data-eventId="'+ eventsJSON[i]['id'] +'" data-indexNum="'+ i +'" data-eventTimestamp="'+ eventsJSON[i]['timestamp'] +'">' +
                          // Event
                          '<span class="title">'+ eventsJSON[i]['title'] +'</span>' +
                          '<span class="body">'+ eventsJSON[i]['body'] +'</span>' +
                          // Comments
                          '<span class="commentContainer"><span class="glyphicon glyphicon-comment eventComment"></span><span class="commentCount">'+ commentCount +'</span></span>' +
                          // Admin only buttons
                          '<div class="btn-group">' +
                            thisEditButton +
                            adminIdeasBtns +
                          '</div>' +
                          // Voting
                          voteHTML +
                        '</div>';

      // check to make sure next index exists
      if (eventsJSON[(i + 1)]) {
        // test next index for new date - if so close div.thisEventDateGroup
        if (thisEventTime !== timeConverter(eventsJSON[(i + 1)]['timestamp'])) {
          eventIdeasHTML += '</div>';
        }
      } else {
        // if next index does not exist - close div.thisEventDateGroup
        eventIdeasHTML += '</div>';
      }

      // Used to record the last date - for grouping
      lastEventIdeaDate = thisEventTime;
    }
  }
  // Now all our variables should be filled with correct data
  // Next we inject those variables into the dom

  // Inject carousel indicators
  $carousel.children('.carousel-indicators').html(carouselIndicatorHTML);

  // Inject all slides html
  $carousel.children('.carousel-inner').html(allSlidesHTML);

  // Inject event ideas html
  $('#eventIdeaList').html(eventIdeasHTML);

  // Test for no active slides and fix if there is no active slide
  if (!$('.carousel-inner div').hasClass('active')) {
    // Adds active class to first slide item
    // $('.carousel-inner div:nth-child(1), .carousel-indicators li:nth-child(1)').addClass('active');

    // Inject active slide that contains a message for the users to vote and approve the next event
    allSlidesHTML += '<div id="slide'+ i +'" class="item active">' +
                        '<div class="slideContainer" data-indexNum="'+ (approvedEventCount + 1) +'">' +
                          // Event
                          '<h4>HEY BITCHES - You need an event for next time!</h4>' +
                          '<p>propose an event and vote upon it</p>' +
                        '</div>' +
                     '</div>';

    carouselIndicatorHTML += '<li data-target="#carousel-events" data-slide-to="'+ (approvedEventCount + 1) +'" class="active"></li>';

    // This SUCKS BTW .. terribly inefficient
    // Inject carousel indicators
    $carousel.children('.carousel-indicators').html(carouselIndicatorHTML);

    // Inject all slides html
    $carousel.children('.carousel-inner').html(allSlidesHTML);
  }

  // Detect no events and show messages
  if (approvedEventCount === 0) {
    $carousel.hide();
    $('.carouselNoItems').html('<h4 class="noEventsHeading">No Approved Events - vote on some event ideas</h4>');
  } else {
    $carousel.show();
    $('.carouselNoItems').html('');
  }

  // If has no event ideas
  if (eventIdeaCount === 0) {
    $('#eventIdeaList').html('<h4 class="noEventsHeading">No Event Ideas - add an event idea </h4>');
  }

  // SHITTY HOTFIXES - yeah ok
  // Hide dates that are already in active carousel
  $('.eventIdeaDate').each(function() {
    var ideaDate = $(this).text();

    for (var i = 0; i < activeEventDates.length; i++) {

      if (ideaDate === activeEventDates[i]) {
        // hide thisEventDateGroup
        $(this).parent().parent().addClass('hide hidenEventIdea');
        // Show - old date button
        $('.toggleOldEventsBtn').removeClass('hide');
      }
    }
  });
}

/* User Presence */
function initializeUserPresence() {
  // Prompt the user for a name to use.
  var name = username,
      currentStatus = "★ online";

  // Get a reference to the presence data in Firebase.
  var userListRef = new Firebase("https://manplanner.firebaseio.com/onlineUsers");

  // Generate a reference to a new location for my user with push.
  var myUserRef = userListRef.push();

  // Get a reference to my own presence status.
  var connectedRef = new Firebase("https://manplanner.firebaseio.com/.info/connected");
  connectedRef.on("value", function(isOnline) {
    if (isOnline.val()) {
      // If we lose our internet connection, we want ourselves removed from the list.
      myUserRef.onDisconnect().remove();

      // Set our initial online status.
      setUserStatus("★ online");
    } else {

      // We need to catch anytime we are marked as offline and then set the correct status. We
      // could be marked as offline 1) on page load or 2) when we lose our internet connection
      // temporarily.
      setUserStatus(currentStatus);
    }
  });

  // A helper function to let us set our own state.
  function setUserStatus(status) {
    // Set our status in the list of online users.
    currentStatus = status;
    myUserRef.set({ name: name, status: status });
  }

  function getMessageId(snapshot) {
    return snapshot.name().replace(/[^a-z0-9\-\_]/gi,'');
  }

  // Update our GUI to show someone"s online status.
  userListRef.on("child_added", function(snapshot) {
    var user = snapshot.val(),
        newColorObject = getRandomColor(true);

    while (!checkForDupeHue(newColorObject.hue)) {
      newColorObject = getRandomColor(true);
    }

    activeUsers[user.name.toLowerCase()] = {
      "name": user.name.toLowerCase(),
      "color": newColorObject.color,
      "hue": newColorObject.hue
    };

    $("<div/>")
      .attr("id", getMessageId(snapshot))
      .html('<span class="activeUser">' + user.name + "</span> is currently " + user.status)
      .appendTo("#presenceDiv");

    markUsersNames();
  });

  // Update our GUI to remove the status of a user who has left.
  userListRef.on("child_removed", function(snapshot) {
    $("#presenceDiv").children("#" + getMessageId(snapshot))
      .remove();
  });

  // Update our GUI to change a user"s status.
  userListRef.on("child_changed", function(snapshot) {
    var user = snapshot.val();
    $("#presenceDiv").children("#" + getMessageId(snapshot))
      .html('<span class="activeUser">' + user.name + "</span> is currently " + user.status);

    markUsersNames();
  });

  // Use idle/away/back events created by idle.js to update our status information.
  document.onIdle = function () {
    setUserStatus("☆ idle");
  };
  document.onAway = function () {
    setUserStatus("☄ away");
  };
  document.onBack = function (isIdle, isAway) {
    setUserStatus("★ online");
  };

  // User is typing - this is kinda shitty
  $('#messageInput').keyup(function(event) {
    var message = $(this).val();

    // trim whitespace from begining and end of string
    message.replace(/^\s\s*/, '').replace(/\s\s*$/, '');

    if(message.length > 1) {
      setUserStatus("Typing..");
    } else {
      setUserStatus("★ online");
    }
  });


  setIdleTimeout(15000); // 15 seconds
  setAwayTimeout(60000); // 1 min
}

/* CHAT */
function initializeFirebaseChat() {
  if (username !== '') {

    // When the user presses enter on the message input, write the message to firebase.
    $('#messageInput').keypress(function (e) {
      if (e.keyCode == 13) {
        sendMessage();
      }
    });
    // for send message btn
    $('#sendChatMessage').click(function(event) {
      event.preventDefault();
      sendMessage();
    });

    // Add a callback that is triggered for each chat message.
    messagesRef.limit(20).on('child_added', function (snapshot) {
      var message = snapshot.val();

      // prevent online users json from showing up in chat
      if (typeof message.name === "undefined") {
        return '';
      }

      $('<div/>')
        .text(message.text)
        .prepend($('<span class="name" data-username="'+  message.name +'"/>').text( message.name +': '))
        .appendTo($('#messagesDiv'));

      $('#messagesDiv')[0].scrollTop = $('#messagesDiv')[0].scrollHeight;

      // insure new messages are marked
      markUsersNames();
      imRecievedSound();
    });

    scrollToBottomOfChat();
  }
}

function sendMessage() {
  var name = username;
  var text = $('#messageInput').val();
  messagesRef.push({name:name, text:text});
  $('#messageInput').val('');
}

function scrollToBottomOfChat() {
  var $messagesDivChildren = $('#messagesDiv div'),
      numberOfMessages = $messagesDivChildren.length;

  if (numberOfMessages === 0) {
    // wait for messages to appear
    var messagesInterval = setInterval(function() {
      if ($('#messagesDiv div').length > 0) {
        // messages have now arived
        clearInterval(messagesInterval);
        calculateAndScroll();
        markUsersNames();
      }
    }, 100);
  } else {
    calculateAndScroll();
  }

  function calculateAndScroll() {
    var messageHeight = $('#messagesDiv div').height(),
        numberOfMessages = $('#messagesDiv div').length,
        endScrollPosition = messageHeight * numberOfMessages;

    $('#messagesDiv').animate({
      scrollTop: endScrollPosition + 'px'
    }, 400);
  }
}

function imRecievedSound() {
  // test last row for existance of username
  var lastMessageUsername = $('#messagesDiv div:last span.name').attr('data-username');

  if (canPlaySound && lastMessageUsername !== username) {
    audioTag.src = imRecievedSoundPath;
    audioTag.play();
  }

  // to prevent first loaded messages from making sounds
  if (!canPlaySound) {
    setTimeout(function() {
      canPlaySound = true;
    }, 4000);
  }
}

function markUsersNames() {
  // iterate through activeUsers object to find what color should be for this usersname
  $('#messagesDiv div .name, .activeUser').each(function() {
    var thisUserName = $(this).text().toLowerCase(),
        newColor;

    thisUserName = thisUserName.replace(': ', '');

    // detect missing object and fix
    if (typeof activeUsers[thisUserName] === 'undefined') {
      var newColorObject = getRandomColor(true);

      while (!checkForDupeHue(newColorObject.hue)) {
        newColorObject = getRandomColor(true);
      }

      activeUsers[thisUserName] = {
        "name": thisUserName.toLowerCase(),
        "color": newColorObject.color,
        "hue": newColorObject.hue
      };
    }

    newColor = activeUsers[thisUserName]['color'];

    // no color yet - get one
    if (!newColor) {
      newColor = getRandomColor(true);

      while (!checkForDupeHue(newColor.hue)) {
        newColor = getRandomColor(true);
      }

      activeUsers[thisUserName]['color'] = newColor.color;
      activeUsers[thisUserName]['hue'] = newColor.hue;
    }

    $(this).attr('style', 'color: '+ newColor +';');

    // bold username of users that do not match js variable - username
    if (thisUserName !== username.toLowerCase()) {
      $(this).addClass('bold');
    }
  });
}

function checkForDupeHue(thisHue) {
  var returnResult = true;

  $.each(activeUsers, function(index, val) {

    var hueVariance = 15,
        thisMaxValue = activeUsers[index]['hue'] + hueVariance;
        thisMinValue = activeUsers[index]['hue'] - hueVariance;

    if (thisHue > thisMaxValue) {
      // console.log('good hue value');
    } else if (thisHue < thisMinValue) {
      // console.log('good hue value');
    } else {
      returnResult = false;
    }
  });

  // return boolean
  return returnResult;
}

function getRandomColor(returnHueBool) {
  // note this function only gets a random hue - saturation and lightness are hard coded
  var randomHue = Math.floor(Math.random() * (360 - 0)) + 0;

  // for returning HUE
  if (returnHueBool) {
    return { "color": "hsl("+ randomHue +", 75%, 35%)", "hue": randomHue };
  }

  return 'hsl('+ randomHue +', 75%, 35%)';
}

function timeConverter(UNIX_timestamp, returnSlashedDate){
  var a = new Date(UNIX_timestamp*1000),
      months = ['Jan','Feb','Mar','Apr','May','Jun','Jul','Aug','Sep','Oct','Nov','Dec'],
      days = ['Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday'],
      year = a.getFullYear(),
      month = months[a.getMonth()],
      date = a.getDate(),
      dayOfWeek = days[a.getDay()],
      time = dayOfWeek +' '+ date+','+month+' '+year;

  if (returnSlashedDate) {
    year = a.getFullYear();
    month = a.getMonth() + 1;
    date = a.getDate();
    dayOfWeek = a.getDay();
    time = month + '/' + date + '/' + year;
  }

  return time;
}