$( document ).ready(function() {
  $('#inputEmailLogin').focus();

  // Submit sign up form
  $('#signUpForm, #loginForm').submit(function(e) {
    e.preventDefault();

    var thisFormId = $(this).attr('id');

    // Prevent duplicate form submission
    $('#' + thisFormId + ' .formSubmit').addClass('active').attr('disabled', 'disabled');

    if (validateSignUp(thisFormId)) {
      // Form passed validation
      if (thisFormId === 'loginForm') {
        logInUser();
      } else {
        signUpUser();
      }
    }
    // Reset button
    $('#' + thisFormId + ' .formSubmit').removeClass('active').removeAttr('disabled');
  });

  // Sign up or login button
  $('.signUpLoginBtnGroup .btn').click(function() {
    $('.signUpLoginBtnGroup .btn').removeClass('active');
    $(this).addClass('active');

    $('#signUpForm').toggleClass('slideLeft');
    $('#loginForm').toggleClass('slideLeft');

    // TODO - fix these animations
    if ($(this).attr('data-action') === 'signup') {
      // Show sign up
      $('#inputEmail').focus();
    } else {
      // Show login
      $('#inputEmailLogin').focus();
    }
  });

  // Place localStorage username into input if exists
  restoreStoredUsername();
});


/* FUNCTIONS
 *
 */


/* Local storage */
function cacheUsername(thisUsername) {
  if (localStorage) {
    localStorage.setItem('username', thisUsername);
  }
}

function restoreStoredUsername() {
  if (localStorage) {
    var cachedUsername = localStorage.getItem('username');

    if (cachedUsername && cachedUsername !== "0") {
      $('#inputEmailLogin').val(cachedUsername);

      // since email is filled - focus password input
      $('#inputPasswordLogin').focus();
    }
  }
}

function logInUser() {
  var email = $('#loginForm .email').val(),
      password = $('#loginForm .password').val();

  $.post("ajax-lib/login.php", { username: email, pw: password })
  .done(function(data) {
    if (data === 'success') {
      displayAlert('success', '<b>You are now logged in</b>', 6000);

      // Set thisUsername in local storage
      cacheUsername(email);

      setTimeout(function() {
        window.location = '';
      }, 1400);
    } else {
      displayAlert('danger', '<b>Login Failed</b> - incorrect username or password', 6000);

      // Clear & focus password
      $('#loginForm .password').val('').focus();
    }
  });
}

function signUpUser() {
  var email = $('#signUpForm .email').val(),
      password = $('#signUpForm .password').val(),
      nickname = $('#signUpForm .nickname').val();

  $.post("ajax-lib/signUp.php", { username: email, pw: password, nickname: nickname })
  .done(function(data) {
    if (data === 'success') {
      displayAlert('success', '<b>You are now signed up and logged in</b>', 6000);

      setTimeout(function() {
        window.location = '';
      }, 1400);
    } else {
      displayAlert('danger', '<b>Sign Up Failed</b> - this username is probably already taken', 6000);

      // Clear inputs & focus email
      $('#signUpForm .email').val('').focus();
      $('#signUpForm .password').val('');
    }
  });
}

function validateSignUp(thisForm) {
  var email = $('#' + thisForm + ' .email').val(),
      password = $('#' + thisForm + ' .password').val();

  if (email.length < 3) {
    displayAlert('danger', 'email address cannot be blank', 6000);
    return false;
  }
  if (password.length < 5) {
    displayAlert('danger', 'password must be at least 6 characters', 6000);
    return false;
  }
  return true;
}

function displayAlert(type, message, duration) {
  /*  Type - success, info, warning, danger
   *  Message - any html
   *  Duration - time in MS to display alert for
   *
   *  Example:
   *    displayAlert('danger', 'some html', 5000);
   */

  // scroll to top of page
  $("html, body").animate({ scrollTop: 0 }, "slow");

  $('#mainAlert')
    .removeClass()
    .addClass('alert alert-' + type)
    .html(message)
    .slideDown();

  // hide on click
  $('#mainAlert').click(function() {
    $(this).slideUp();
  });

  if (duration) {
    setTimeout(function() {
      $('#mainAlert').slideUp();
    }, duration);
  }
}