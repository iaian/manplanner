-- phpMyAdmin SQL Dump
-- version 3.5.1
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Oct 17, 2013 at 10:49 PM
-- Server version: 5.5.25
-- PHP Version: 5.4.4

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";

--
-- Database: `manplanner`
--

-- --------------------------------------------------------

--
-- Table structure for table `events`
--

CREATE TABLE `events` (
  `id` int(20) NOT NULL AUTO_INCREMENT,
  `username` varchar(250) COLLATE utf8_unicode_ci NOT NULL,
  `timestamp` int(20) NOT NULL,
  `title` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `body` varchar(2000) COLLATE utf8_unicode_ci NOT NULL,
  `comments` varchar(5000) COLLATE utf8_unicode_ci NOT NULL,
  `isChosenEvent` int(2) NOT NULL,
  `votesFor` varchar(1000) COLLATE utf8_unicode_ci NOT NULL,
  `votesAgainst` varchar(1000) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=18 ;

--
-- Dumping data for table `events`
--

INSERT INTO `events` (`id`, `username`, `timestamp`, `title`, `body`, `comments`, `isChosenEvent`, `votesFor`, `votesAgainst`) VALUES
(1, 'iaianrw@gmail.com', 1381881600, 'Bowling at Holiday Lanes', 'This is some body text. Yo Yo Yo', '', 1, '["iaianrw@gmail.com"]', '[]'),
(2, 'iaianrw@gmail.com', 1382486400, '2nd EVENT', '2nd event body', '', 1, 'null', '["iaianrw@gmail.com"]'),
(4, 'iaianrw@gmail.com', 1381347932, 'an old event', 'SOME OLD EVENT TXT', '', 1, '', ''),
(14, 'iaianrw@gmail.com', 0, 'Not Approved Event', 'not approved event WEASDAFASDSAD', '', 0, '["iaianrw@gmail.com"]', 'null'),
(16, 'iaianrw@gmail.com', 0, 'Yodle event idea', 'HEY HEY HEY', '', 0, '["iaianrw@gmail.com"]', 'null'),
(17, 'iaianrw@gmail.com', 0, 'TEsting ', 'Your mom', '', 0, '["iaianrw@gmail.com"]', 'null');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(20) NOT NULL AUTO_INCREMENT,
  `username` varchar(250) COLLATE utf8_unicode_ci NOT NULL,
  `nickname` varchar(250) COLLATE utf8_unicode_ci NOT NULL,
  `pw` varchar(300) COLLATE utf8_unicode_ci NOT NULL,
  `isAdmin` int(2) NOT NULL,
  UNIQUE KEY `id` (`id`),
  UNIQUE KEY `username` (`username`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=33 ;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `username`, `nickname`, `pw`, `isAdmin`) VALUES
(30, 'iaianrw@gmail.com', 'Iaian', '$2a$12$h7dBjDykUKPYsaHzwbfP4O/riFuwpK75HhmwMqg2Gmcg8fZjdf9g.', 1),
(31, 't@t.c', 'testUser2', '$2a$12$h7dBjDykUKPYsaHzwbfP4O/riFuwpK75HhmwMqg2Gmcg8fZjdf9g.', 0),
(32, 'Test@test.c', 'testUser-1', '$2a$12$h7dBjDykUKPYsaHzwbfP4O/riFuwpK75HhmwMqg2Gmcg8fZjdf9g.', 0);
